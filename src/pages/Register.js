import {useState, useEffect, useContext} from	'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Register() {

	//avtivity 5
	const {user} = useContext(UserContext);

	const history = useNavigate();
	
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	//simulation of user registration
	function registerUser(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: "Email Already Exists",
					icon: "error",
					text: "Please provide another email"
				})
			} else {
				fetch('http://localhost:4000/users/register', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data === true) {
						setFirstName('');
						setLastName('');
						setMobileNo('');
						setEmail('');
						setPassword('');

						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "Welcome to Zuitt"
						})

						history("/login")
					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again"
						})
					}
				})
			}
		})

		
	}

	useEffect(() => {
		if(firstName !== '' && lastName !== '' && mobileNo !== '' && email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password]);

	return(
		
		//activity 5
		(user.id !== null) ?
		<Navigate to="/courses"/> :

		<Form onSubmit={e => registerUser(e)}>
			<h1>Register</h1>
			
			<Form.Group className="mb-3" controlId="firstname">
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter your First Name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="lastname">
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter your Last Name"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="mobileno">
				<Form.Label>Mobile No.</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter your Mobile No."
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="my-3" controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter your email here" 
					value={email}
					onChange={e => setEmail(e.target.value)} 
					required
				/>
				<Form.Text className="text-muted">We will never share your email with anyone else.</Form.Text>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Enter your password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			

		{/*conditionally rendering submit button based on isActive state*/}
			{ 
				isActive ?
				<Button variant="primary" type="submit" className="mb-5" id="submitBtn">
				Submit
				</Button>
				:
				<Button variant="danger" type="submit" className="mb-5" id="submitBtn" disabled>
				Submit
				</Button>
			}
		</Form>
	);
};