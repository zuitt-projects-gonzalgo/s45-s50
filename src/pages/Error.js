import {Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import '../App'

export default function Error() {
	return(
		<Row>
			<Col>
				<h1 className="error">404: Page Not Found</h1>
				<p className="error">Go back to <Link to="/">Home Page</Link></p>
			</Col>
		</Row>
	)
}