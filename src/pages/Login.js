import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login(props) {

	//useContext
	const {user, setUser} = useContext(UserContext);


	//useState
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	//simulation of user registration
	function authenticate(e) {
		e.preventDefault();
		//clear input fields

		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt'
				})
			} else {
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Please check your credentials'
				})
			}
		})

		//set the email of the authenticated user in the local storage
		//syntax: localStorage.setItem("key", value)
		// localStorage.setItem("email", email);

		//to access user information by using localStorage. 
		//when state change components are rerender and the AppNavbar component will be updated based on the user credentials
		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details', {
				method: "POST",
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}

		setEmail('');
		setPassword('');

		// alert('You are now logged in');
		
	}

	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);

	return(
		(user.id !== null) ?
		<Navigate to="/courses"/> :

		<Form onSubmit={e => authenticate(e)}>
			<h1 className="mt-5">Login</h1>
			<Form.Group className="my-3" controlId="loginUserEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter your email here" 
					value={email}
					onChange={e => setEmail(e.target.value)} 
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="loginPassword">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Enter your password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

		{/*conditionally rendering submit button based on isActive state*/}
			{ 
				isActive ?
				<Button variant="success" type="submit" className="mb-5" id="submitLoginBtn">
				Submit
				</Button>
				:
				<Button variant="danger" type="submit" className="mb-5" id="submitLoginBtn" disabled>
				Submit
				</Button>
			}
			<p>Does not have an account?</p>
			<Link to="/register">Create Account</Link>
		</Form>
	);
}