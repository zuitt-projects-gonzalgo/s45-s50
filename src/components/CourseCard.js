import {useState, useEffect} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function CourseCard({courseProp}) {

	//syntax: const [getter, setter] = useState(initialValueofGetter)
	// const [count, setCount] = useState(0);
	// const [seat, setSeat] = useState(30);	

// A C T I V I T Y - 2

	// function enroll() {
	// 	if(count < 30 && seat > 0) {
	// 		setCount(count + 1);
	// 		setSeat(seat - 1);
	// 	} /*else {
	// 		alert(`Max Enrollees: Cannot Enroll in ${name} anymore`);
	// 	}*/
	// };

	//syntax: useEffect(() => {}, [optionalParameter])
	// useEffect(() => {
	// 	if(seat === 0) {
	// 		alert(`Max Enrollees: Cannot Enroll in ${name} anymore`);
	// 	}
	// }, [seat])

	const {name, description, price, _id} = courseProp;
	return(
		<Row>
			<Col xs={12} md={12}>
				<Card className="p-3 mb-3">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Button variant="primary" as={Link} to={`/courses/${_id}`}>See Details</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
};